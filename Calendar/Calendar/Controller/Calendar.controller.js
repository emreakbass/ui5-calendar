sap.ui.define([
          'sap/ui/core/mvc/Controller',
          'sap/ui/model/json/JSONModel',
          'sap/ui/core/Fragment',
          'jquery.sap.global',
          'sap/m/MessageBox',
          'sap/m/MessageToast'
     ],
     function (Controller, JSONModel, Fragment, jQuery, MessageBox, MessageToast) {
          "use strict";

          var oData = oModel.getData();
          var _UserCreateController = sap.ui.controller("SapUI5Calendar.Calendar.UserCreate.Controller.UserCreate");
          var _LoginController = sap.ui.controller("SapUI5Calendar.Calendar.Login.Controller.Login");

          var PageController = Controller.extend("SapUI5Calendar.Calendar.Calendar.Controller", {

               onInit: function () {
                    // this.onAction();
                    _UserCreateController.onAction();
                    this.getView().setModel(oModel);

                    _UserCreateController.userControl();
                    //kullanıcı leader değilse giriş yaptırmamaya yarar.
                    var _leaderControl = _LoginController.leaderControl();
                    if (_leaderControl === false) {
                         this.getView().byId("header-userCreate").setVisible(false);
                    }
                    _UserCreateController.userList();
                    // CalendarService._dropTable();
                    // this.firstAppointment();
               },
               //Model de planları set etmeye yarar.
               planList: function (i, _userMail, ) {
                    CalendarService._planList(_userMail).then(fullfilled => {
                         var j = i;
                         jQuery.each(fullfilled.rows, function (_index, value) {
                              var parcalanacakSD = value.startDate;
                              var parcalanacakEN = value.endDate;
                              var parcaStartDate = parcalanacakSD.split(",")
                              var parcaEndDate = parcalanacakEN.split(",");
                              var json = {
                                   id: value.calendar_id,
                                   start: new Date(parcaStartDate[0], parcaStartDate[1] - 1, parcaStartDate[2], "00", "00"),
                                   end: new Date(parcaEndDate[0], parcaEndDate[1] - 1, parcaEndDate[2], "23", "59"),
                                   title: value.tittle,
                                   text: value.text,
                                   type: value.type,
                                   empMail: value.empMail,
                                   tentative: false,
                                   textStartDate: value.startDate,
                                   textEndDate: value.endDate,
                              }
                              oData.user.people[j].appointments.push(json);
                              oModel.setData(oData);
                         })

                    }).catch(rejected => {
                         console.log(rejected);
                    })
               },
               //kullanıcı oluşturma sayfasına yönlendirir.
               UserCreateProcessing: function () {
                    var adres = "/#/UserCreate";
                    sap.m.URLHelper.redirect(adres, false);
                    window.location.reload(false);
               },

               //login sayfasına yönlendirir.
               Calendar_LoginProcessing: function () {
                    localStorage.setItem("LoginUserMail", "");
                    localStorage.setItem("LoginUserPosition", "");
                    var adres = "/#/";
                    sap.m.URLHelper.redirect(adres, false);
                    window.location.reload(false);
               },

               //kullanıcı yetkisine göre kendisine veya başkasına takvim eklemesini kontrol eder.
               createDialogShowControl: function (oEvent) {
                    if (_LoginController.timeControl() == false) {
                         var LoginUserMail = localStorage.getItem("LoginUserMail");
                         var LoginUserPosition = localStorage.getItem("LoginUserPosition");
                         var userMail = oEvent.getParameters().row.mProperties.text;
                         //create fragment i açılırken tıklanılan günün tarihini alması için kullanıldı.
                         var tarih = new Date();
                         var gun = String(oEvent.mParameters.startDate.getDate());
                         var ay = String(parseInt(oEvent.mParameters.startDate.getMonth()) + 1);
                         var yil = String(oEvent.mParameters.startDate.getFullYear());
                         var saat = String(tarih.getHours());
                         var dakika = String(tarih.getMinutes());
                         var createStart = {
                              "tarih": tarih,
                              "gun": gun,
                              "ay": ay,
                              "yil": yil,
                              "saat": saat,
                              "dakika": dakika
                         }
                         oModel.setProperty("/createStart", createStart);

                         if (LoginUserPosition == "Leader") {
                              this.loadCreateDialogFragment(oEvent);
                              this.createFragmentClear();
                         } else {
                              if (LoginUserMail == userMail) {
                                   this.loadCreateDialogFragment();
                                   this.createFragmentClear();
                              } else {
                                   sap.m.MessageBox.alert(
                                        "Diğer kullanıcılara plan ekleme yetkiniz bulunmamaktadır.", {
                                             icon: sap.m.MessageBox.Icon.ERROR,
                                             title: "Plan Ekleme Hatası"
                                        }
                                   );
                              }
                         }
                    } else {
                         _LoginController.timeControlYonlendirme();
                    }
               },

               //kullanıcı yetkisine göre kendisine veya başkasının takvimini düzenlemesini kontrol eder.
               detailsDialogShowControl: function (oEvent) {
                    if (_LoginController.timeControl() == false) {
                         var LoginUserMail = localStorage.getItem("LoginUserMail");
                         var LoginUserPosition = localStorage.getItem("LoginUserPosition");
                         var _key = oEvent.getParameters().appointment.mProperties.key;
                         var key = _key.split(",");
                         //key[0]=>> kullanıcının mailini ifade eder.
                         //key[0]=>> appointment id sini ifade eder.
                         if (LoginUserPosition == "Leader") {
                              this.loadDetailsDialogFragment(key[1]);
                         } else {
                              if (LoginUserMail == key[0]) {
                                   this.loadDetailsDialogFragment(key[1]);
                              } else {
                                   sap.m.MessageBox.alert(
                                        "Diğer kullanıcıların planını düzenleme yetkiniz bulunmamaktadır.", {
                                             icon: sap.m.MessageBox.Icon.ERROR,
                                             title: "Plan Düzenleme Hatası"
                                        }
                                   );
                              }
                         }
                    } else {
                         _LoginController.timeControlYonlendirme();
                    }
               },

               //takvim ekleme dialogunu açar.
               loadCreateDialogFragment: function (oEvent) {

                    if (!this._oCreateDialog) {
                         this._oCreateDialog = sap.ui.xmlfragment("SapUI5Calendar.Calendar.Calendar.Fragment.Create", this);
                         this.getView().addDependent(this._oCreateDialog);
                    }
                    debugger;
                    this._oCreateDialog.open();
                    this.createFragmentClear();

                    var yil = oModel.oData.createStart.yil;
                    var ay = oModel.oData.createStart.ay;
                    var gun = oModel.oData.createStart.gun;
                    var saat = oModel.oData.createStart.saat;
                    var dakika = oModel.oData.createStart.dakika;

                    sap.ui.core.BusyIndicator.show(0);
                    jQuery.sap.delayedCall(200, this, function () {
                         sap.ui.core.BusyIndicator.hide();
                         sap.ui.getCore().byId("createStartDate").setValue(yil + "," + ay + "," + gun + "," + saat + "," + dakika);
                         sap.ui.getCore().byId("createEndDate").setValue(yil + "," + ay + "," + gun + ",23,59");
                    });
               },

               //takvim ekleme dialogunu kapatır.
               closeCreateDialogFragment: function () {
                    this._oCreateDialog.close();
               },

               //takvim detay dailogunu açar.
               loadDetailsDialogFragment(id) {
                    var appointmentId = id;
                    localStorage.setItem("selectedAppointmentId", appointmentId);
                    CalendarService._planSearch(appointmentId).then(fullfilled => {
                         if (!this._oDetailsDialog) {
                              this._oDetailsDialog = sap.ui.xmlfragment("SapUI5Calendar.Calendar.Calendar.Fragment.Details", this);
                              this.getView().addDependent(this._oDetailsDialog);
                         }

                         this._oDetailsDialog.open();
                         sap.ui.core.BusyIndicator.show(0);
                         jQuery.sap.delayedCall(300, this, function () {
                              sap.ui.core.BusyIndicator.hide();
                         });
                         var type;
                         jQuery.each(fullfilled.rows, function (_index, value) {

                              sap.ui.getCore().byId("detailsTitle").setValue(value.tittle);
                              sap.ui.getCore().byId("detailsText").setValue(value.text);
                              sap.ui.getCore().byId("detailsStartDate").setValue(value.startDate);
                              sap.ui.getCore().byId("detailsEndDate").setValue(value.endDate);
                              type = value.type;
                         });
                         sap.ui.getCore().byId("detailsType").setSelectedKey(type);


                    }).catch(rejected => {

                    })

               },
               //takvim dialogunu kapatır.
               closeDetailsDialogFragment: function () {
                    this._oDetailsDialog.close();

               },
               //crate fragmentindeki inputları ilk değerine döndürür.
               createFragmentClear: function () {
                    sap.ui.getCore().byId("createTitle").setValue("");
                    sap.ui.getCore().byId("createText").setValue("");
                    sap.ui.getCore().byId("createStartDate").setValue("");
                    sap.ui.getCore().byId("createEndDate").setValue("");
                    sap.ui.getCore().byId("createType").setSelectedKey("Type01");
               },
               //appointment ekler.
               calendarCreate: function () {
                    var title = sap.ui.getCore().byId("createTitle").getValue();
                    var text = sap.ui.getCore().byId("createText").getValue();
                    var startDate = sap.ui.getCore().byId("createStartDate").getValue();
                    var endDate = sap.ui.getCore().byId("createEndDate").getValue();
                    var tentative = "false";
                    var type = sap.ui.getCore().byId("createType").getSelectedKey();
                    var empMail = this.getView().byId("plngCal").getSelectedRows()["0"].mProperties.text;

                    console.log(title);
                    console.log(text);
                    console.log(startDate);
                    console.log(endDate);
                    console.log(tentative);
                    console.log(type);
                    console.log(empMail);

                    CalendarService._calendarCreate(startDate, endDate, title, text, tentative, type, empMail).then(fullfilled => {
                         console.log("Oluşturulan kayıt:");
                         console.log(fullfilled);
                         MessageToast.show("Plan oluşturuldu.")
                         this.closeCreateDialogFragment();
                         _UserCreateController.userList();
                    }).catch(rejected => {
                         console.log("Kayıt oluşturulamadı.");
                         console.log("Oluşan Hata: ", rejected)
                         sap.m.MessageBox.alert(
                              "Zorunlu alanların girildiğinden emin olun.", {
                                   icon: sap.m.MessageBox.Icon.ERROR,
                                   title: "Zorunlu Alan Hatası"
                              }
                         );
                         this.createFragmentClear();
                    })
               },
               firstAppointment: function () {
                    var title = "deneme";
                    var text = "deneme";
                    var startDate = "2019,05,02,10,44";
                    var endDate = "2019,05,02,15,44";
                    var tentative = "false";
                    var type = "Type04";
                    var empMail = "eakbas@gmail.com";

                    console.log(title);
                    console.log(text);
                    console.log(startDate);
                    console.log(endDate);
                    console.log(tentative);
                    console.log(type);
                    console.log(empMail);

                    CalendarService._calendarCreate(startDate, endDate, title, text, tentative, type, empMail).then(fullfilled => {
                         console.log("Oluşturulan kayıt:");
                         console.log(fullfilled);
                         MessageToast.show("Plan oluşturuldu.")
                         this.closeCreateDialogFragment();
                         _UserCreateController.userList();
                    }).catch(rejected => {
                         console.log("Kayıt oluşturulamadı.");
                         console.log("Oluşan Hata: ", rejected)
                         sap.m.MessageBox.alert(
                              "Zorunlu alanların girildiğinden emin olun."
                         );
                         this.createFragmentClear();
                    })
               },
               //appointment düzenler.
               calendarUpdate: function () {
                    var id = localStorage.getItem("selectedAppointmentId");
                    var title = sap.ui.getCore().byId("detailsTitle").getValue();
                    var text = sap.ui.getCore().byId("detailsText").getValue();
                    var startDate = sap.ui.getCore().byId("detailsStartDate").getValue();
                    var endDate = sap.ui.getCore().byId("detailsEndDate").getValue();
                    var type = sap.ui.getCore().byId("detailsType").getSelectedKey();

                    CalendarService._calendarUpdate(title, text, type, startDate, endDate, id).then(fullfilled => {
                         console.log("Düzenlenen kayıt:");
                         console.log(fullfilled);
                         MessageToast.show("Plan düzenlendi.");
                         _UserCreateController.userList();
                         this.closeDetailsDialogFragment();
                    }).catch(rejected => {
                         sap.m.MessageBox.alert(
                              rejected, {
                                   icon: sap.m.MessageBox.Icon.ERROR,
                                   title: "Zorunlu Alan Hatası"
                              }
                         );
                         console.log("hata: ", rejected);
                    })

               },
               //appointment siler
               calendarDelete: function () {
                    var id = localStorage.getItem("selectedAppointmentId");
                    CalendarService._calendarDelete(id).then(fullfilled => {
                         console.log("Silinen kayıt:");
                         console.log(fullfilled);
                         MessageToast.show("Plan silindi.")
                         _UserCreateController.userList();
                         this.closeDetailsDialogFragment();
                    }).catch(rejected => {
                         console.log("hata: ", rejected);
                    })
               }
          });

          return PageController;

     });