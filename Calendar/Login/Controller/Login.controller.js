sap.ui.define([
     'jquery.sap.global',
     'sap/ui/core/mvc/Controller',
     'sap/m/MessageBox',
], function (jQuery, Controller, MessageBox) {
     'use strict';
     return Controller.extend("SapUI5Calendar.Calendar.Login.Controller", {
          onInit: function (oEvent) {
               UserService._userCreateTable();
               CalendarService._calendarCreateTable();
               // UserService._userFirstCreate();
          },
          //kullanıcı kaydının olup olmadığını kontrol eder.
          loginControl: function (evt) {
               var kullaniciAdi = this.getView().byId("login-input-kullaniciAdi").getValue();
               var sifre = this.getView().byId("login-input-KullaniciSifre").getValue();
               UserService._loginControl(kullaniciAdi, sifre).then(fullfilled => {
                    var tarih = new Date();
                    var saat = tarih.getHours();
                    var dakika = tarih.getMinutes();
                    localStorage.setItem("LoginUserMail", fullfilled.rows[0].user_mail);
                    localStorage.setItem("LoginEnterTimeHours", saat);
                    localStorage.setItem("LoginEnterTimeMinutes", dakika);
                    localStorage.setItem("LoginUserPosition", fullfilled.rows[0].user_position)
                    this.userCreateYonlendir();
               }).catch(rejected => {
                    sap.m.MessageBox.alert(
                         "Kullanıcı bulunamadı. Tekrar deneyiniz.", {
                              icon: sap.m.MessageBox.Icon.ERROR,
                              title: "Hesap Hatası"
                         }
                    );
                    console.log(rejected);
                    this.getView().byId("login-input-KullaniciSifre").setValue("");
                    this.getView().byId("login-input-kullaniciAdi").setValue("");
               })
          },
          //sadece adminin girebileceği kullanıcı kayıt sayfasına yönlendirir.
          userCreateYonlendir: function () {
               var position = localStorage.getItem("LoginUserPosition");
               if (position === "Leader") {
                    var adres = "/#/UserCreate";
                    sap.m.URLHelper.redirect(adres, false);
                    window.location.reload(false);
               } else {
                    var adres = "/#/Calendar";
                    sap.m.URLHelper.redirect(adres, false);
                    window.location.reload(false);
               }

          },
          //position'un leader olup olmadığı kontrol edilir.
          leaderControl: function () {
               var position = localStorage.getItem("LoginUserPosition");
               if (position === "Leader") {
                    return true;
               } else {
                    return false;
               }
          },
          //saat ve dakika kontrolü yaparak 1 saatten sonra sistemden düşürür.
          timeControl: function () {
               var hour = parseInt(localStorage.getItem("LoginEnterTimeHours"));
               var minute = parseInt(localStorage.getItem("LoginEnterTimeMinutes"));
               hour = hour + 1;
               var tarih = new Date();
               var hourControl = tarih.getHours();
               var minuteControl = tarih.getMinutes();

               //test için 
               // minuteControl = minute + 1;
               // hourControl = hour;

               if (hour === hourControl && minuteControl >= minute) {
                    sap.m.MessageBox.alert(
                         "Oturum süreniz dolmuştur. Lütfen tekrar giriş yapınız.", {
                              icon: sap.m.MessageBox.Icon.WARNING,
                              title: "Oturum Kontrol"
                         }
                    );
                    return true;
               } else {
                    return false;
               }
          },
          timeControlYonlendirme: function () {
               setTimeout(function () {
                    var adres = "/#/";
                    sap.m.URLHelper.redirect(adres, false);
                    window.location.reload(false);
                    localStorage.setItem("LoginUserMail", "");
                    localStorage.setItem("LoginUserPosition", "");
               }, 1000);
          }
     });
});