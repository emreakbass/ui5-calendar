var CalendarService = {
    _calendarCreateTable: function () {
        if (document.readyState) {
            databaseOpen();
            veritabani.transaction(function (fx) {
                fx.executeSql('CREATE TABLE IF NOT EXISTS calendar (calendar_id INTEGER PRIMARY KEY autoincrement ,startDate VARCHAR(20), endDate VARCHAR2(20),  tittle ' +
                    'VARCHAR2(50), text VARCHAR2(50), tentative VARCHAR2(15), type VARCHAR2(15),empMail VARCHAR2(30))', [],
                    function (transaction, result) {
                        console.log("tablo oluşturuluyor..")
                        console.log(result);
                    },
                    function (transaction, error) {
                        console.log(error);
                    })
            })
        } else {
            var msg = "tablo oluşturulamadı.";
            reject(msg)
        }
    },
    _calendarCreate: function (startDate, endDate, tittle, text, tentative, type, empMail) {
        return new Promise((resolve, reject) => {
            if (startDate != "" && startDate != null && endDate != "" && endDate != null && tittle != "" && tittle != null &&
                tentative != "" && tentative != null && type != "" && type != null && empMail != "" && empMail != null) {
                veritabani.transaction(function (fx) {
                    fx.executeSql('INSERT INTO calendar (startDate, endDate, tittle,text, tentative, type, empMail) VALUES (?,?,?,?,?,?,?)', [startDate, endDate, tittle, text, tentative, type, empMail], function (transaction, result) {
                            resolve(result)
                        }),
                        function (transaction, error) {
                            reject(new Error(error));
                        };
                })
            } else {
                var msg = "Doldurulması zorunlu alanları doldurunuz.";
                reject(msg)
            }
        })

    },
    _planList: function (userMail) {
        return new Promise((resolve, reject) => {
            veritabani.transaction(function (fx) {
                fx.executeSql('SELECT * FROM calendar WHERE empMail=?', [userMail], function (transaction, result) {
                    resolve(result);
                }, function (transaction, error) {
                    reject(error);
                })
            })
        })
    },
    _planSearch: function (appointmentId) {
        return new Promise((resolve, reject) => {
            veritabani.transaction(function (fx) {
                fx.executeSql('SELECT * FROM calendar WHERE calendar_id=?', [appointmentId], function (transaction, result) {
                    resolve(result);
                }, function (transaction, error) {
                    reject(error);
                })
            })
        })
    },
    _calendarUpdate: function (tittle, text, type, startDate, endDate, id) {
        return new Promise((resolve, reject) => {
            if (startDate != "" && startDate != null && endDate != "" && endDate != null && tittle != "" && tittle != null &&
                type != "" && type != null) {
                veritabani.transaction(function (fx) {
                    fx.executeSql('UPDATE calendar SET tittle =?, text=? , type=? , startDate=? ,endDate=? WHERE calendar_id=?', [tittle, text, type, startDate, endDate, id], function (transaction, result) {
                        resolve(result);
                    }, function (transaction, error) {
                        reject(error);
                    })
                })
            } else {
                var msg = "Doldurulması zorunlu alanları doldurunuz."
                reject(msg);
            }
        })
    },
    _calendarDelete: function (id) {
        return new Promise((resolve, reject) => {
            veritabani.transaction(function (fx) {
                fx.executeSql('DELETE FROM calendar WHERE calendar_id=?', [id], function (transaction, result) {
                    resolve(result);
                }, function (transaction, error) {
                    reject(error);
                })
            })
        })

    },
    _dropTable: function () {
        veritabani.transaction(function (fx) {
            fx.executeSql('drop table calendar', [], function (transaction, result) {
                console.log("tablo düşürüldü.");
            })
        })
    }
}