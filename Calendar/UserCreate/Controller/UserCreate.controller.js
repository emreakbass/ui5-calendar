sap.ui.define([
     'sap/ui/core/mvc/Controller',
     'sap/m/MessageBox',
     'sap/m/MessageToast'
], function (Controller, MessageBox, MessageToast) {
     'use strict';
     var oData = oModel.getData();
     var _LoginController = sap.ui.controller("SapUI5Calendar.Calendar.Login.Controller.Login");
     var _resim = "";
     var fotograf = "";
     return Controller.extend("SapUI5Calendar.Calendar.UserCreate.Controller", {
          onInit: function (oEvent) {
               this.getView().setModel(oModel);
               this.onAction();


               // _LoginController.timeControl();


               //kullanıcı leader değilse giriş yaptırmamaya yarar.
               var _leaderControl = _LoginController.leaderControl();
               if (_leaderControl === false) {
                    // var pathname = window.location.pathname;
                    var adres = "/#/Calendar";
                    sap.m.URLHelper.redirect(adres, false);
                    window.location.reload(false);
               }
               this.userControl();
               ///user model
               this.userList();
               oModel.setProperty("/imageSrcModel", "");

          },
          //kullanıcının çıış yaptıktan sonra geri tuşuna basarak sisteme girişini engeller.
          userControl: function () {
               var userMail = localStorage.getItem("LoginUserMail");
               if (userMail == "") {
                    var adres = "/#/";
                    sap.m.URLHelper.redirect(adres, false);
                    window.location.reload(false);
               }
          },
          //kullanıcıların bilgilerini ve appointmentlerini almak için kullanılır.
          //önce kullanıcıları set edip sonrasında boş oluşturulan appointment dizisine calendar tablosundan  oluşturulan planları çekmek için
          //calendar controllera gidip oradaki planlist fonksiyonu tetiklenir.

          userList: function () {
               var users = {
                    startDate: new Date("2019", "4", "31", "8", "0"),
                    people: []
               };
               oModel.setProperty('/user', users);
               var _CalendarController = sap.ui.controller("SapUI5Calendar.Calendar.Calendar.Controller.Calendar");
               var i = 0;
               UserService._userList().then(fullfilled => {
                    jQuery.each(fullfilled.rows, function (_index, value) {
                         var _users = {
                              userName: value.user_name,
                              userSurname: value.user_surname,
                              userPosition: value.user_position,
                              userMail: value.user_mail,
                              userPhoto: value.user_photo,
                              userPassword: value.user_password,
                              userPosition: value.user_position,
                              appointments: []
                         };
                         oModel.oData.user.people.push(_users)
                         _CalendarController.planList(i, value.user_mail);
                         i++;
                    })
                    console.log("model user: ", users);
               }).catch(rejected => {
                    console.log(rejected);
               })
          },
          //takvim sayfasına yönlendirir.
          UserCreate_CalendarPageProcessing: function () {
               var adres = "/#/Calendar";
               sap.m.URLHelper.redirect(adres, false);
               window.location.reload(false);
          },
          //login sayfasına yönlendirir.
          UserCreate_LoginProcessing: function () {
               var adres = "/#/";
               sap.m.URLHelper.redirect(adres, false);
               window.location.reload(false);
               localStorage.setItem("LoginUserMail", "");
               localStorage.setItem("LoginUserPosition", "");

          },
          //kullanıcı ekler.
          userCreateControl: function () {
               var ad = this.getView().byId("userCreate-input-ad").getValue();
               var soyAd = this.getView().byId("userCreate-input-soyad").getValue();
               var mail = this.getView().byId("userCreate-input-mail").getValue();
               var sifre = this.getView().byId("userCreate-input-sifre").getValue();
               fotograf = oModel.getProperty("/imageSrcModel");
               var pozisyon = this.getView().byId("userCreate-select-pozisyon").getSelectedKey();
               console.log(ad, soyAd, mail, sifre, fotograf, pozisyon);
               UserService._userCreateControl(ad, soyAd, mail, sifre, fotograf, pozisyon).then(fullfilled => {
                    if (fullfilled == true) {
                         UserService._userCreate(ad, soyAd, mail, sifre, fotograf, pozisyon).then(fullfilled => {
                              console.log(fullfilled);
                              MessageToast.show("Kayıt oluşturuldu.")
                              this.userList();
                              this.userCreateTextClear();

                         }).catch(rejected => {
                              console.log(rejected);
                              this.userCreateTextClear();
                         })
                    }
               }).catch(rejected => {
                    sap.m.MessageBox.alert(
                         rejected, {
                              icon: sap.m.MessageBox.Icon.ERROR,
                              title: "Kullanıcı Oluşturma Hatası"
                         }
                    );
                    console.log(rejected)
               })
          },
          //kullanıcı eklendikten sonra textbox ları boşaltır.
          userCreateTextClear: function () {
               this.getView().byId("userCreate-input-ad").setValue("");
               this.getView().byId("userCreate-input-soyad").setValue("");
               this.getView().byId("userCreate-input-mail").setValue("");
               this.getView().byId("userCreate-input-sifre").setValue("");
               this.getView().byId("fileUploader").setValue("");
               this.getView().byId("userCreate-select-pozisyon").setSelectedKey("Consultant");
               localStorage.setItem("mailUpdate", "");
          },
          //kullanının üzerine tıklandığında bilgilerini inputlara yazdırır.
          onPress: function (oEvent) {
               _LoginController.timeControl();
               var deneme = _LoginController.timeControl();
               if (deneme == false) {
                    var oModelContext = oEvent.getParameter("listItem").getBindingContext().getObject();
                    console.log(oModelContext);
                    localStorage.setItem("mailUpdate", oModelContext.userMail);
                    this.getView().byId("userCreate-input-ad").setValue(oModelContext.userName);
                    this.getView().byId("userCreate-input-soyad").setValue(oModelContext.userSurname);
                    this.getView().byId("userCreate-input-mail").setValue(oModelContext.userMail);
                    this.getView().byId("userCreate-input-sifre").setValue(oModelContext.userPassword);
                    this.getView().byId("fileUploader").setValue(oModelContext.userPhoto);
                    this.getView().byId("userCreate-select-pozisyon").setSelectedKey(oModelContext.userPosition);
               } else {
                    _LoginController.timeControlYonlendirme();
               }
          },
          //kullanıcı bilgilerini düzenler.
          userUpdate: function () {
               var _mailUpdate = localStorage.getItem("mailUpdate");
               var ad = this.getView().byId("userCreate-input-ad").getValue();
               var soyAd = this.getView().byId("userCreate-input-soyad").getValue();
               var mail = this.getView().byId("userCreate-input-mail").getValue();
               var sifre = this.getView().byId("userCreate-input-sifre").getValue();
               fotograf = oModel.getProperty("/imageSrcModel");
               var pozisyon = this.getView().byId("userCreate-select-pozisyon").getSelectedKey();
               this.handleUploadPress();
               UserService._userUpdate(ad, soyAd, mail, sifre, fotograf, pozisyon, _mailUpdate).then(fullfilled => {
                    console.log(fullfilled);
                    MessageToast.show("Kayıt düzenlendi.");
                    this.userList();

               }).catch(rejected => {
                    sap.m.MessageBox.alert(
                         rejected, {
                              icon: sap.m.MessageBox.Icon.ERROR,
                              title: "Kayıt Düzenleme Hatası"
                         }
                    );
                    console.log(rejected);
                    this.userCreateTextClear();

               })
          },
          //kullanıcı siler.
          userDelete: function () {
               var mail = this.getView().byId("userCreate-input-mail").getValue();
               UserService._userDelete(mail).then(fullfilled => {
                    console.log(fullfilled);
                    MessageToast.show("Kayıt silindi.")


                    this.userList();
                    this.userCreateTextClear();
               }).catch(rejected => {
                    sap.m.MessageBox.alert(
                         rejected, {
                              icon: sap.m.MessageBox.Icon.ERROR,
                              title: "Kayıt Silme Hatası"
                         }
                    );
                    console.log(rejected);
                    this.userCreateTextClear();

               })
          },
          onAction: function (oEvt) {
               sap.ui.core.BusyIndicator.show(0)
               jQuery.sap.delayedCall(1000, this, function () {
                    sap.ui.core.BusyIndicator.hide();
               });
          },
          handleUploadPress: function (oEvent) {
               var oFileUploader = this.byId("fileUploader");
               var domRef = oFileUploader.getFocusDomRef();
               var file = domRef.files[0];
               var that = this;
               this.fileName = file.name;
               this.fileType = file.type;
               var reader = new FileReader();
               reader.readAsDataURL(file);

               reader.onload = function () {
                    _resim = reader.result;
                    oModel.setProperty("/imageSrcModel", _resim);
               }
          },
          handleTypeMissmatch: function (oEvent) {
               var aFileTypes = oEvent.getSource().getFileType();
               jQuery.each(aFileTypes, function (key, value) {
                    aFileTypes[key] = "*." + value;
               });
               var sSupportedFileTypes = aFileTypes.join(", ");
               MessageToast.show("The file type *." + oEvent.getParameter("fileType") +
                    " is not supported. Choose one of the following types: " +
                    sSupportedFileTypes);
          },
          handleUploadComplete: function (oEvent) {
               debugger;
               var sResponse = oEvent.getParameter("response");
               if (sResponse) {
                    var sMsg = "";
                    var m = /^\[(\d\d\d)\]:(.*)$/.exec(sResponse);
                    if (m[1] == "200") {
                         sMsg = "Return Code: " + m[1] + "\n" + m[2] + "(Upload Success)";
                         oEvent.getSource().setValue("");
                    } else {
                         sMsg = "Return Code: " + m[1] + "\n" + m[2] + "(Upload Error)";
                    }
                    MessageToast.show(sMsg);
               }
          },
          handleValueChange: function (oEvent) {
               this.handleUploadPress();
          },

     });
});