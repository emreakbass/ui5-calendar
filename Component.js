'use strict'
jQuery.sap.require('SapUI5Calendar.Router')
sap.ui.define(
     [
          'sap/ui/core/UIComponent',
          // 'sap/ui/model/json/JSONModel'
     ],
     function (UIComponent, JSONModel) {
          return UIComponent.extend('SapUI5Tutorial.Component', {
               metadata: {
                    routing: {
                         config: {
                              routerClass: SapUI5Calendar.Router,
                              viewType: 'XML',
                              targetAggregation: 'pages',
                              clearTarget: false
                         },
                         routes: [{
                                   pattern: '',
                                   viewPath: 'SapUI5Calendar.Calendar.Login.View',
                                   title: 'Login',
                                   name: 'Login',
                                   view: 'Login',
                                   targetControl: 'masterAppView'
                              },
                              {
                                   pattern: 'UserCreate',
                                   viewPath: 'SapUI5Calendar.Calendar.UserCreate.View',
                                   title: 'UserCreate',
                                   name: 'UserCreate',
                                   view: 'UserCreate',
                                   targetControl: 'masterAppView'
                              },
                              {
                                   pattern: 'Calendar',
                                   viewPath: 'SapUI5Calendar.Calendar.Calendar.View',
                                   title: 'Calendar',
                                   name: 'Calendar',
                                   view: 'Calendar',
                                   targetControl: 'masterAppView'
                              },
                              {
                                   pattern: 'deneme',
                                   viewPath: 'SapUI5Calendar.Calendar.deneme',
                                   title: 'deneme',
                                   name: 'deneme',
                                   view: 'deneme',
                                   targetControl: 'masterAppView'
                              }
                         ]
                    }
               },
               init: function () {
                    sap.ui.core.UIComponent.prototype.init.apply(this, arguments);
                    var mConfig = this.getMetadata().getConfig();
                    this.getRouter().initialize();
               },
               createContent: function () {
                    var oViewData = {
                         component: this
                    }
                    return sap.ui.view({
                         viewName: 'SapUI5Calendar.RootApp',
                         type: sap.ui.core.mvc.ViewType.XML,
                         id: 'app',
                         viewData: oViewData
                    })
               }
          })
     }
)