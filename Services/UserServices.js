var _LoginController = sap.ui.controller("SapUI5Calendar.Calendar.Login.Controller.Login");
var UserService = {
     _userCreateTable: function () {
          if (document.readyState) {
               databaseOpen();
               veritabani.transaction(function (fx) {
                    fx.executeSql('CREATE TABLE IF NOT EXISTS users (user_id INTEGER PRIMARY KEY autoincrement , user_name VARCHAR2(30), user_surname VARCHAR2(30), user_mail ' +
                         'VARCHAR2(40), user_password VARCHAR2(15),user_photo VARCHAR2(40),user_position VARCHAR2(15))', [],
                         function (transaction, result) {
                              console.log(result);
                         },
                         function (transaction, error) {
                              console.log(error);
                         })
               })
          } else {
               var msg = "sayfa yüklenemedi";
               reject(msg)
          }
     },
     _userFirstCreate: function () {
          veritabani.transaction(function (fx) {
               var user_name = "Emre";
               var user_surname = "AKBAŞ";
               var user_mail = "eakbas1@gmail.com";
               var user_password = "1234";
               var user_photo = "leader.png"
               var user_position = "Leader";
               fx.executeSql('INSERT INTO users (user_name,user_surname,user_mail,user_password, user_photo,user_position) VALUES (?,?,?,?,?,?)', [user_name, user_surname, user_mail, user_password, user_photo, user_position],
                         function (transaction, result) {
                              console.log(result);
                              var _CalendarController = sap.ui.controller("SapUI5Calendar.Calendar.Calendar.Controller.Calendar");
                              _CalendarController.firstAppointment();
                         }),
                    function (transaction, error) {
                         console.log(error);

                    };
          })
     },
     _loginControl: function (nickname, password) {
          return new Promise((resolve, reject) => {
               if (nickname != "" && nickname != null && password != "" && password != null) {
                    veritabani.transaction(function (fx) {
                         fx.executeSql('SELECT * FROM users WHERE user_mail=? AND user_password=?', [nickname, password], function (transaction, result) {
                              if (result.rows.length > 0) {
                                   resolve(result)
                              } else {
                                   var err = "Hatalı deneme. Lütfen tekrar deneyiniz.";
                                   reject(err);
                              }
                         });
                    });
               } else {
                    var msg = "lütfen bilgileri tam ve eksiksiz giriniz.";
                    reject(msg)
               }
          })
     },
     _userCreateControl: function (ad, soyAd, mail, sifre, fotograf, pozisyon) {
          return new Promise((resolve, reject) => {
               if (ad != "" && ad != null && soyAd != "" && soyAd != null && mail != "" &&
                    mail != null && sifre != "" && sifre != null && fotograf != "" && fotograf != null &&
                    pozisyon != "" && pozisyon != null) {
                    if (_LoginController.timeControl() == false) {
                         veritabani.transaction(function (fx) {
                              fx.executeSql('SELECT * FROM users', [], function (transaction, result) {
                                   if (result.rows.length == 0) {
                                        resolve(true);
                                   }
                                   jQuery.each(result.rows, function (_index, value) {
                                        if (mail === value.user_mail) {
                                             var msg = "denendiğiniz maiil adresi ile kayıt bulunmaktadır. lütfen başka kullanıcı adi ile kayıt olmayı deneyiniz.";
                                             reject(msg);
                                        } else {
                                             resolve(true);
                                        }
                                   });
                              }, function (transaction, error) {
                                   var err = "Hata: " + error;
                                   reject(err);
                              });
                         })
                    } else {
                         _LoginController.timeControlYonlendirme();
                    }
               } else {
                    var msg = "Lütfen bilgileri tam ve eksiksiz giriniz.";
                    reject(msg);
               }
          })
     },
     _userCreate: function (ad, soyAd, mail, sifre, fotograf, pozisyon) {
          return new Promise((resolve, reject) => {
               veritabani.transaction(function (fx) {
                    fx.executeSql('INSERT INTO users (user_name,user_surname,user_mail,user_password,user_photo,user_position) VALUES (?,?,?,?,?,?)', [ad, soyAd, mail, sifre, fotograf, pozisyon], function (transaction, result) {
                              resolve(result)
                         }),
                         function (transaction, error) {
                              reject(new Error(error));
                         };
               })
          })

     },
     _userList: function () {
          return new Promise((resolve, reject) => {
               // _LoginController.timeControl();
               veritabani.transaction(function (fx) {
                    fx.executeSql('SELECT * FROM users', [], function (transaction, result) {
                         resolve(result);
                    }, function (transaction, error) {
                         reject(error);
                    })
               })
          })
     },
     _userUpdate: function (ad, soyAd, mail, sifre, fotograf, pozisyon, updateMail) {
          return new Promise((resolve, reject) => {
               if (ad != "" && ad != null && soyAd != "" && soyAd != null && mail != "" &&
                    mail != null && sifre != "" && sifre != null && fotograf != "" && fotograf != null &&
                    pozisyon != "" && pozisyon != null) {
                    _LoginController.timeControl();
                    if (_LoginController.timeControl() == false) {
                         veritabani.transaction(function (fx) {
                              fx.executeSql('UPDATE users SET user_name=? ,user_surname=?, user_mail=? ,user_password=? ,user_photo=? ,user_position=? WHERE user_mail=?', [ad, soyAd, mail, sifre, fotograf, pozisyon, updateMail], function (transaction, result) {
                                        resolve(result)
                                   }),
                                   function (transaction, error) {
                                        var msg = "düzenlenmeye çalışılan kullanıcının var olduğundan emin olup lütfen tekrar deneyiniz."
                                        // reject(new Error(error));
                                        reject(msg);
                                   };
                         })
                    } else {
                         _LoginController.timeControlYonlendirme();
                    }
               } else {
                    var msg = "Düzenlemek istediğiniz kayıtı seçiniz.";
                    reject(msg)
               }
          })
     },
     _userDelete: function (mail) {
          return new Promise((resolve, reject) => {
               if (mail != "" && mail != null) {
                    if (_LoginController.timeControl() == false) {
                         veritabani.transaction(function (fx) {
                              fx.executeSql('DELETE FROM users WHERE user_mail=?', [mail], function (transaction, result) {
                                   resolve(result);
                              }, function (transaction, error) {
                                   var err = "Hata: " + error;
                                   reject(err);
                              });
                         })
                    } else {
                         _LoginController.timeControlYonlendirme();
                    }
               } else {
                    var msg = "Silmek istediğiniz kayıtı seçiniz.";
                    reject(msg);
               }
          })
     },

}